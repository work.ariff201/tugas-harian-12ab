<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $name = $request['First_Name'];
        $name2 = $request['Last_Name'];
        return view('welcome', compact('name', 'name2'));
    }
}
