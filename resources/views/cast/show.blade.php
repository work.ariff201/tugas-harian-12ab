@extends('layout.master')

@section('judul')
Cast {{$cast->nama}} Detail
@endsection

@section('content')

<h2>{{$cast->nama}}</h2>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

@endsection