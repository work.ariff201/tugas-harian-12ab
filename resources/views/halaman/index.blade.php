@extends('layout.master')

@section('judul')
Register
@endsection

@section('content')
    <h1>
        Buat Account Baru!
    </h1>
    <h2>
        Sign Up Form
    </h2>
    <form action="/welcome" method="post">
    @csrf
    <label>First name:</label>
    <p>
        <input type="text" name="First Name"/>
    </p>
    <p>
        <label>Last name:</label>
        <p><input type="text" name="Last Name"/></p>
    </p>
    <a href="/welcome"><input type="submit" name="Sign Up" value="Sign Up"></a>    
@endsection